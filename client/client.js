'use strict';

angular.module('clientApp', [
        'ngAnimate',
        'ngResource',
        'ui.router',
        'ngMaterial',
        'LocalStorageModule',
        'angular-loading-bar',
        'ngMessages',
        'underscore',
        

        'app.config',
        'mod.nbos',
        'mod.idn',
        'mod.app',
          'mod.m34'
])

angular.module('clientApp')
.constant('CLIENT_CONFIG',{
        CLIENT_ID: '82951f81-8fe2-498c-bf91-a646a9717587',
        CLIENT_SECRET: 'webApp',
        CLIENT_DOMAIN : 'http://localhost:9001'
})
